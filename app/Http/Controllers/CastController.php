<?php

namespace App\Http\Controllers;

use App\Models\Cast;
use Illuminate\Http\Request;

class CastController extends Controller
{
    public function index()
    {
        $casts = Cast::all();
        return view('cast.index', compact('casts'));
    }

    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'age' => 'required|integer',
            'bio' => 'required',
        ]);

        Cast::create($request->all());

        return redirect()->route('cast.index')->with('success', 'Cast created successfully.');
    }

    public function show(Cast $cast)
    {
        return view('cast.show', compact('cast'));
    }

    public function edit(Cast $cast)
    {
        return view('cast.edit', compact('cast'));
    }

    public function update(Request $request, Cast $cast)
    {
        $request->validate([
            'name' => 'required',
            'age' => 'required|integer',
            'bio' => 'required',
        ]);

        $cast->update($request->all());

        return redirect()->route('cast.index')->with('success', 'Cast updated successfully.');
    }

    public function destroy(Cast $cast)
    {
        $cast->delete();

        return redirect()->route('cast.index')->with('success', 'Cast deleted successfully.');
    }
}
