<?php

use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;

Route::get('/', [CastController::class, 'index']);
Route::resource('cast', CastController::class);

